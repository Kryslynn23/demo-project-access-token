#!/bin/bash
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --header "Content-Type:application/json" \
     --data '{ "name":"🤖token", "scopes":["api", "read_repository"], "expires_at":"2022-01-31" }' \
     "https://gitlab.com/api/v4/projects/25883585/access_tokens"
